#!/bin/bash
# variable section ------------------------------------------------------------------------------
HOUR=`date +%H`
WEEK=`date +%A`
MONTH=`date +%Y-%d`
MYSQL_USER=wordpress
MYSQL_PASSWORD=salamdonya
MYSQL_ROOT_USER=root
MYSQL_ROOT_PASSWORD=salamdonya
Restore_Logs=/var/log/mysql-restore.log

FILE=DockerMe-Mysql.sql

# run maridb temporary container
docker run -id --name tmp_db_restore_test \
      -e MYSQL_ROOT_PASSWORD=salamdonya \
      -e MYSQL_DATABASE=wordpress \
      -e MYSQL_USER=wordpress \
      -e MYSQL_PASSWORD=salamdonya \
      mysql:5.7

sleep 15
#sleep 90
# restore mailserver backup
cat $FILE | docker exec -i tmp_db_restore_test mysql -u $MYSQL_ROOT_USER -p$MYSQL_ROOT_PASSWORD wordpress

# check restore backup
TABEL_COUNT=`docker exec -i tmp_db_restore_test mysql -u $MYSQL_USER -p$MYSQL_PASSWORD <<< "show databases; use wordpress; show tables;" | wc -l`
USER_COUNT=`docker exec -i tmp_db_restore_test mysql -u $MYSQL_USER -p$MYSQL_PASSWORD  <<< "use wordpress; SELECT * FROM wp_users;" | wc -l`
p=0
echo "Mysql Wordpress db tabel Count = $TABEL_COUNT" >>$Restore_Logs
if [ "$TABEL_COUNT" -gt 5 ]; then echo "Mysql Wordpress db tabel Count is ok" >> $Restore_Logs && ((p++)) ; else echo "Mysql Wordpress db tabel Count is NOT ok"  >> $Restore_Logs ; fi
echo "Mysql Wordpress db Mailbox user Count = $USER_COUNT" >>$Restore_Logs
if [ "$USER_COUNT" -gt 1 ]; then echo "Mysql Wordpress db Mailbox user Count is ok" >> $Restore_Logs && ((p++)) ; else echo "Mysql Wordpress db Mailbox user Count is NOT ok"  >> $Restore_Logs ; fi

# remove maridb temporary container
docker rm -f tmp_db_restore_test

echo -e "# Mysql Restore Result: -------------------------------------------------------" >> $Restore_Logs
if [ "$p" == 2 ]; then echo "Mysql Backup Restore Successful" >> $Restore_Logs && p=0 ; else echo "Mysql Backup Restore Unsuccessful nad EXIT"  >> $Restore_Logs && `(echo "Subject: $SUBJECT_RESTORE"; echo "From: $FROM"; echo "To:$TO"; echo ""; echo -e "$MESSAGE_RESTORE") | ssmtp $TO` && exit 0 ; fi
echo -e "# Mysql Recovery End Time: `date +%Y-%m-%d__%H:%M:%S` -------------------------------------" >> $Restore_Logs
echo >> $Restore_Logs

cat $Restore_Logs
# the end ----------------------------------------------------------------------------------------------------
